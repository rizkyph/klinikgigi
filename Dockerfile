# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
FROM alpine:latest
LABEL maintainer="rizky.hustamely@bankmuamalat.co.id"
VOLUME /main-app
ADD build/libs/klinikgigi-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar","/app.jar"]
CMD ["/bin/sh"]
