package com.rizky.klinikgigi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KlinikgigiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KlinikgigiApplication.class, args);
	}

}
