/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizky.klinikgigi.controller;

import com.rizky.klinikgigi.exception.ResourceNotFoundException;
import com.rizky.klinikgigi.model.Doctor;
import com.rizky.klinikgigi.repository.DoctorRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Rizky Pratama <rizky.hustamely@bankmuamalat.co.id>
 */
@RestController
@RequestMapping("/api/v1")
public class DoctorController {
    
    @Autowired
    private DoctorRepository doctorRepo;
    
    @GetMapping("/doctors")
    public List<Doctor> getAllDoctor() {
        return doctorRepo.findAll();
    }
    
    @GetMapping("/doctors/{id}")
    public ResponseEntity<Doctor> getDoctorById(@PathVariable(value = "id") Long doctorId) throws ResourceNotFoundException {
        Doctor doctor = doctorRepo.findById(doctorId)
            .orElseThrow(() -> new ResourceNotFoundException("Doctor not found for this id :: " + doctorId));
        
        return ResponseEntity.ok().body(doctor);
    }
    
    @PostMapping("/doctors")
    public Doctor createDoctor(@Valid @RequestBody Doctor doctor) {
        return doctorRepo.save(doctor);
    }
    
    @PutMapping("/doctors/{id}")
    public ResponseEntity<Doctor> updateDoctor(@PathVariable(value = "id") Long doctorId,
            @Valid @RequestBody Doctor doctorDetails) throws ResourceNotFoundException {
        Doctor doctor = doctorRepo.findById(doctorId)
            .orElseThrow(() -> new ResourceNotFoundException("Doctor not found for this id :: " + doctorId));

        doctor.setDoctorName(doctorDetails.getDoctorName());
        
        final Doctor updatedDoctor = doctorRepo.save(doctor);
        return ResponseEntity.ok(updatedDoctor);
    }
    
    @DeleteMapping("/doctor/{id}")
    public Map<String, Boolean> deleteDoctor(@PathVariable(value = "id") Long doctorId) throws ResourceNotFoundException {
        Doctor doctor = doctorRepo.findById(doctorId)
            .orElseThrow(() -> new ResourceNotFoundException("Doctor not found for this id :: " + doctorId));

        doctorRepo.delete(doctor);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
