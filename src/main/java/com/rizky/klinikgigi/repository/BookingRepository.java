/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizky.klinikgigi.repository;

import com.rizky.klinikgigi.model.Booking;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rizky Pratama <rizky.hustamely@bankmuamalat.co.id>
 */
@Repository
public interface BookingRepository extends JpaRepository<Booking, String>{
    
    
}
