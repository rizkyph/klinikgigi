/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizky.klinikgigi.repository;

import com.rizky.klinikgigi.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rizky Pratama <rizky.hustamely@bankmuamalat.co.id>
 */
@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long>{
    
}
