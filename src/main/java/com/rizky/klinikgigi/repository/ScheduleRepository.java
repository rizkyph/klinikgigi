/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizky.klinikgigi.repository;

import com.rizky.klinikgigi.model.Schedule;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rizky Pratama <rizky.hustamely@bankmuamalat.co.id>
 */
@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    
    @Query("select u.doctor_id from schdules u where u.schedule_day = ?1")
    Schedule findScheduleByDoctorId(String dayName);
}
